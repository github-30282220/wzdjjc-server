package com.ntkjxy.wzdjjc.utils;

import com.ntkjxy.wzdjjc.model.Device;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ExcelUtil {

    /**
     * 读取Excel里面的内容
     * @param filePath
     * @return
     */
    public List<Device> readExcelContent(String filePath) {
        File file = new File(filePath);
        Workbook wb = getExcelWorkbook(new File(filePath));
        List<Device> deviceList = new ArrayList<>();
        // 拿到第一个sheet
        Sheet sheet = wb.getSheetAt(0);
        // 得到Excel的行数
        int totalRows = sheet.getPhysicalNumberOfRows();
        int totalCells = 0;
        // 得到Excel的列数，前提是有行数
        if(totalRows > 1 && sheet.getRow(0) != null) {
            totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
        }
        for (int r = 2; r < totalRows; r++) {
            Row row = sheet.getRow(r);
            if(row == null) {
                continue;
            }
            Device device = new Device();
            int rowColNum = row.getPhysicalNumberOfCells();
            for (int c = 0; c < rowColNum; c++) {
                Cell cell = row.getCell(c);

                if (cell != null) {
                    int cellType = cell.getCellType();
                    switch (cellType) {
                        case HSSFCell.CELL_TYPE_NUMERIC:
                            double numValue = cell.getNumericCellValue();
                            if(c == 6) {
                                device.setLongitude(BigDecimal.valueOf(numValue));
                            } else if(c == 7) {
                                device.setLatitude(BigDecimal.valueOf(numValue));
                            }
//                            System.out.println(cell.getNumericCellValue());
                            break;
                        case HSSFCell.CELL_TYPE_STRING:
                            String strValue = cell.getStringCellValue();
                            if (c == 1) {
                                device.setDeviceName(strValue);
                            } else if(c == 5) {
                                device.setPhone(strValue);
                                System.out.println(cell.getStringCellValue());
                            } else if(c == 8) {
                                device.setDeviceIp(strValue);
                            } else if(c == 9) {
                                device.setDeviceUsername(strValue);
                            } else if(c == 10) {
                                device.setDevicePwd(strValue);
                            }


                            break;
                        case HSSFCell.CELL_TYPE_BOOLEAN:
                            System.out.println(cell.getBooleanCellValue());
                            break;
                    }
                }
            }
            deviceList.add(device);
        }
        return deviceList;
    }

    /**
     * 获取workbook
     * @param mFile
     * @return
     */
    private Workbook getExcelWorkbook(File mFile) {
        String fileName = mFile.getName();
        Workbook wb = null;
        try {
            if (!validateExcel(fileName)) {
                return null;
            }
            boolean isExcel2003 = true;
            if (isExcel2007(fileName)) {
                isExcel2003 = false;
            }
            wb = createExcelWorkbook(new FileInputStream(mFile), isExcel2003);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return wb;
    }

    /**
     * 根据文件类型创建workbook
     * @param is
     * @param isExcel2003
     * @return
     */
    private Workbook createExcelWorkbook(InputStream is, boolean isExcel2003) {
        Workbook wb = null;
        try {
            wb = isExcel2003 ? new HSSFWorkbook(is) : new XSSFWorkbook(is);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return wb;
    }

    // @描述：是否是2003的excel，返回true是2003
    private static boolean isExcel2003(String filePath) {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    // @描述：是否是2007的excel，返回true是2007
    private static boolean isExcel2007(String filePath) {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }


    /**
     * 验证Excel文件
     * @param fileName
     * @return
     */
    private boolean validateExcel(String fileName) {
        if (fileName == null || !(isExcel2003(fileName) || isExcel2007(fileName))) {
            return false;
        }
        return true;
    }

}
