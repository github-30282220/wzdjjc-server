package com.ntkjxy.wzdjjc.service;
import com.ntkjxy.wzdjjc.model.Device;
import com.ntkjxy.wzdjjc.core.Service;


/**
 * Created by AndyKong on 2018/02/05.
 */
public interface DeviceService extends Service<Device> {

}
