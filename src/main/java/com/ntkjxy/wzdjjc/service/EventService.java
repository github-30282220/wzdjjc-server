package com.ntkjxy.wzdjjc.service;
import com.ntkjxy.wzdjjc.model.Event;
import com.ntkjxy.wzdjjc.core.Service;

import java.util.List;


/**
 * Created by AndyKong on 2018/01/21.
 */
public interface EventService extends Service<Event> {
    public List<Event> selectEventByCondition(Event event);

    public List<Event> selectGroupByCondition(Event event);
}
