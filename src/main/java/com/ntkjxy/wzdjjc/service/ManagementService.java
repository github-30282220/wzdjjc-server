package com.ntkjxy.wzdjjc.service;
import com.ntkjxy.wzdjjc.model.Management;
import com.ntkjxy.wzdjjc.core.Service;


/**
 * Created by AndyKong on 2018/01/21.
 */
public interface ManagementService extends Service<Management> {

}
