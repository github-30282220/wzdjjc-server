package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.ManagementMapper;
import com.ntkjxy.wzdjjc.model.Management;
import com.ntkjxy.wzdjjc.service.ManagementService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by AndyKong on 2018/01/21.
 */
@Service
@Transactional
public class ManagementServiceImpl extends AbstractService<Management> implements ManagementService {
    @Resource
    private ManagementMapper managementMapper;

}
