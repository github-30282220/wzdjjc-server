package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.DeviceMapper;
import com.ntkjxy.wzdjjc.model.Device;
import com.ntkjxy.wzdjjc.service.DeviceService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by AndyKong on 2018/02/05.
 */
@Service
@Transactional
public class DeviceServiceImpl extends AbstractService<Device> implements DeviceService {
    @Resource
    private DeviceMapper deviceMapper;

}
