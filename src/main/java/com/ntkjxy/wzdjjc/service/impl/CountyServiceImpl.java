package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.CountyMapper;
import com.ntkjxy.wzdjjc.model.County;
import com.ntkjxy.wzdjjc.service.CountyService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by AndyKong on 2018/01/21.
 */
@Service
@Transactional
public class CountyServiceImpl extends AbstractService<County> implements CountyService {
    @Resource
    private CountyMapper countyMapper;

}
