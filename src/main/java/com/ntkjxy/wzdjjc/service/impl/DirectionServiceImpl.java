package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.DirectionMapper;
import com.ntkjxy.wzdjjc.model.Direction;
import com.ntkjxy.wzdjjc.service.DirectionService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by AndyKong on 2018/01/21.
 */
@Service
@Transactional
public class DirectionServiceImpl extends AbstractService<Direction> implements DirectionService {
    @Resource
    private DirectionMapper directionMapper;

}
