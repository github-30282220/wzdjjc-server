package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.TownMapper;
import com.ntkjxy.wzdjjc.model.Town;
import com.ntkjxy.wzdjjc.service.TownService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by AndyKong on 2018/01/21.
 */
@Service
@Transactional
public class TownServiceImpl extends AbstractService<Town> implements TownService {
    @Resource
    private TownMapper townMapper;

}
