package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.RouteMapper;
import com.ntkjxy.wzdjjc.model.Route;
import com.ntkjxy.wzdjjc.service.RouteService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by AndyKong on 2018/01/21.
 */
@Service
@Transactional
public class RouteServiceImpl extends AbstractService<Route> implements RouteService {
    @Resource
    private RouteMapper routeMapper;

}
