package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.EventMapper;
import com.ntkjxy.wzdjjc.model.Event;
import com.ntkjxy.wzdjjc.service.EventService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by AndyKong on 2018/01/21.
 */
@Service
@Transactional
public class EventServiceImpl extends AbstractService<Event> implements EventService {
    @Resource
    private EventMapper eventMapper;

    @Override
    public List<Event> selectEventByCondition(Event event) {
        return eventMapper.selectEventByCondition(event);
    }

    @Override
    public List<Event> selectGroupByCondition(Event event) {
        return eventMapper.selectGroupByCondition(event);
    }
}
