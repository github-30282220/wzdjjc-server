package com.ntkjxy.wzdjjc.service.impl;

import com.ntkjxy.wzdjjc.dao.UserMapper;
import com.ntkjxy.wzdjjc.model.User;
import com.ntkjxy.wzdjjc.service.UserService;
import com.ntkjxy.wzdjjc.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by AndyKong on 2018/01/21.
 */
@Service
@Transactional
public class UserServiceImpl extends AbstractService<User> implements UserService {
    @Resource
    private UserMapper userMapper;

}
