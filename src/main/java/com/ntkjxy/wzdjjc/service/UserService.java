package com.ntkjxy.wzdjjc.service;
import com.ntkjxy.wzdjjc.model.User;
import com.ntkjxy.wzdjjc.core.Service;


/**
 * Created by AndyKong on 2018/01/21.
 */
public interface UserService extends Service<User> {

}
