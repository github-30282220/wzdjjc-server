package com.ntkjxy.wzdjjc.model;

import javax.persistence.*;

public class Route {
    @Id
    @Column(name = "route_id")
    private Integer routeId;

    @Column(name = "route_name")
    private String routeName;

    /**
     * @return route_id
     */
    public Integer getRouteId() {
        return routeId;
    }

    /**
     * @param routeId
     */
    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    /**
     * @return route_name
     */
    public String getRouteName() {
        return routeName;
    }

    /**
     * @param routeName
     */
    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }
}