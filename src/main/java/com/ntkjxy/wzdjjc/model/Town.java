package com.ntkjxy.wzdjjc.model;

import javax.persistence.*;

public class Town {
    @Id
    @Column(name = "town_id")
    private Integer townId;

    @Column(name = "town_name")
    private String townName;

    @Column(name = "county_id")
    private Integer countyId;

    /**
     * @return town_id
     */
    public Integer getTownId() {
        return townId;
    }

    /**
     * @param townId
     */
    public void setTownId(Integer townId) {
        this.townId = townId;
    }

    /**
     * @return town_name
     */
    public String getTownName() {
        return townName;
    }

    /**
     * @param townName
     */
    public void setTownName(String townName) {
        this.townName = townName;
    }

    /**
     * @return county_id
     */
    public Integer getCountyId() {
        return countyId;
    }

    /**
     * @param countyId
     */
    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }
}