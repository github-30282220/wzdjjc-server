package com.ntkjxy.wzdjjc.model;

import javax.persistence.*;

public class Direction {
    @Id
    @Column(name = "direction_id")
    private Integer directionId;

    @Column(name = "direction_name")
    private String directionName;

    /**
     * @return direction_id
     */
    public Integer getDirectionId() {
        return directionId;
    }

    /**
     * @param directionId
     */
    public void setDirectionId(Integer directionId) {
        this.directionId = directionId;
    }

    /**
     * @return direction_name
     */
    public String getDirectionName() {
        return directionName;
    }

    /**
     * @param directionName
     */
    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }
}