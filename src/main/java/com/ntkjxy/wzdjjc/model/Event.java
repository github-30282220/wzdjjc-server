package com.ntkjxy.wzdjjc.model;

import java.util.Date;
import javax.persistence.*;

public class Event {
    /**
     * 事件主键
     */
    @Id
    @Column(name = "event_id")
    private Integer eventId;

    /**
     * 设备编号
     */
    @Column(name = "device_id")
    private Integer deviceId;

    /**
     * 预置点
     */
    @Column(name = "preset_point")
    private Integer presetPoint;

    /**
     * 反馈时间
     */
    @Column(name = "feedback_time")
    private Date feedbackTime;

    /**
     * 短信发送时间
     */
    @Column(name = "send_msg_time")
    private Date sendMsgTime;

    /**
     * 报警时间
     */
    @Column(name = "alarm_time")
    private Date alarmTime;

    /**
     * 反馈结果
     */
    @Column(name = "feedback_result")
    private String feedbackResult;

    /**
     * 确认时间
     */
    @Column(name = "confirm_time")
    private Date confirmTime;

    /**
     * 报警图片
     */
    @Column(name = "event_photo")
    private String eventPhoto;

    /**
     * 报警类型
     */
    @Column(name = "event_type")
    private Integer eventType;

    /**
     * 处理结果
     */
    @Column(name = "handle_result")
    private Integer handleResult;

    /**
     * 处理时间
     */
    @Column(name = "handle_time")
    private Date handleTime;

    private Integer page;
    private Integer size;
    @Transient
    private Date alarmStartTime;
    @Transient
    private Date alarmEndTime;

    @Column(name = "handle_count")
    private Integer handleCount;
    @Column(name = "event_count")
    private Integer eventCount;


    @Column(name = "timely_count")
    private Integer timelyCount;

    @Column(name = "untimely_count")
    private Integer untimelyCount;

    @Column(name = "timely_rate")
    private String timelyRate;

    @Column(name = "event_county_name")
    private String eventCountyName;
    @Column(name = "event_town_name")
    private String eventTownName;

    public String getEventCountyName() {
        return eventCountyName;
    }

    public void setEventCountyName(String eventCountyName) {
        this.eventCountyName = eventCountyName;
    }

    public String getEventTownName() {
        return eventTownName;
    }

    public void setEventTownName(String eventTownName) {
        this.eventTownName = eventTownName;
    }

    public Integer getTimelyCount() {
        return timelyCount;
    }

    public void setTimelyCount(Integer timelyCount) {
        this.timelyCount = timelyCount;
    }

    public Integer getUntimelyCount() {
        return untimelyCount;
    }

    public void setUntimelyCount(Integer untimelyCount) {
        this.untimelyCount = untimelyCount;
    }

    public String getTimelyRate() {
        return timelyRate;
    }

    public void setTimelyRate(String timelyRate) {
        this.timelyRate = timelyRate;
    }

    @Transient
    private Integer groupType;

    public Integer getGroupType() {
        return groupType;
    }

    public void setGroupType(Integer groupType) {
        this.groupType = groupType;
    }

    public Integer getHandleCount() {
        return handleCount;
    }

    public void setHandleCount(Integer handleCount) {
        this.handleCount = handleCount;
    }

    public Integer getEventCount() {
        return eventCount;
    }

    public void setEventCount(Integer eventCount) {
        this.eventCount = eventCount;
    }

    public Date getAlarmStartTime() {
        return alarmStartTime;
    }

    public void setAlarmStartTime(Date alarmStartTime) {
        this.alarmStartTime = alarmStartTime;
    }

    public Date getAlarmEndTime() {
        return alarmEndTime;
    }

    public void setAlarmEndTime(Date alarmEndTime) {
        this.alarmEndTime = alarmEndTime;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Transient
    private Device device;

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    /**
     * 获取事件主键
     *
     * @return event_id - 事件主键
     */
    public Integer getEventId() {
        return eventId;
    }

    /**
     * 设置事件主键
     *
     * @param eventId 事件主键
     */
    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    /**
     * 获取设备编号
     *
     * @return device_id - 设备编号
     */
    public Integer getDeviceId() {
        return deviceId;
    }

    /**
     * 设置设备编号
     *
     * @param deviceId 设备编号
     */
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 获取预置点
     *
     * @return preset_point - 预置点
     */
    public Integer getPresetPoint() {
        return presetPoint;
    }

    /**
     * 设置预置点
     *
     * @param presetPoint 预置点
     */
    public void setPresetPoint(Integer presetPoint) {
        this.presetPoint = presetPoint;
    }

    /**
     * 获取反馈时间
     *
     * @return feedback_time - 反馈时间
     */
    public Date getFeedbackTime() {
        return feedbackTime;
    }

    /**
     * 设置反馈时间
     *
     * @param feedbackTime 反馈时间
     */
    public void setFeedbackTime(Date feedbackTime) {
        this.feedbackTime = feedbackTime;
    }

    /**
     * 获取短信发送时间
     *
     * @return send_msg_time - 短信发送时间
     */
    public Date getSendMsgTime() {
        return sendMsgTime;
    }

    /**
     * 设置短信发送时间
     *
     * @param sendMsgTime 短信发送时间
     */
    public void setSendMsgTime(Date sendMsgTime) {
        this.sendMsgTime = sendMsgTime;
    }

    /**
     * 获取报警时间
     *
     * @return alarm_time - 报警时间
     */
    public Date getAlarmTime() {
        return alarmTime;
    }

    /**
     * 设置报警时间
     *
     * @param alarmTime 报警时间
     */
    public void setAlarmTime(Date alarmTime) {
        this.alarmTime = alarmTime;
    }

    /**
     * 获取反馈结果
     *
     * @return feedback_result - 反馈结果
     */
    public String getFeedbackResult() {
        return feedbackResult;
    }

    /**
     * 设置反馈结果
     *
     * @param feedbackResult 反馈结果
     */
    public void setFeedbackResult(String feedbackResult) {
        this.feedbackResult = feedbackResult;
    }

    /**
     * 获取确认时间
     *
     * @return confirm_time - 确认时间
     */
    public Date getConfirmTime() {
        return confirmTime;
    }

    /**
     * 设置确认时间
     *
     * @param confirmTime 确认时间
     */
    public void setConfirmTime(Date confirmTime) {
        this.confirmTime = confirmTime;
    }

    /**
     * 获取报警图片
     *
     * @return event_photo - 报警图片
     */
    public String getEventPhoto() {
        return eventPhoto;
    }

    /**
     * 设置报警图片
     *
     * @param eventPhoto 报警图片
     */
    public void setEventPhoto(String eventPhoto) {
        this.eventPhoto = eventPhoto;
    }

    /**
     * 获取报警类型
     *
     * @return event_type - 报警类型
     */
    public Integer getEventType() {
        return eventType;
    }

    /**
     * 设置报警类型
     *
     * @param eventType 报警类型
     */
    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    /**
     * 获取处理结果
     *
     * @return handle_result - 处理结果
     */
    public Integer getHandleResult() {
        return handleResult;
    }

    /**
     * 设置处理结果
     *
     * @param handleResult 处理结果
     */
    public void setHandleResult(Integer handleResult) {
        this.handleResult = handleResult;
    }

    /**
     * 获取处理时间
     *
     * @return handle_time - 处理时间
     */
    public Date getHandleTime() {
        return handleTime;
    }

    /**
     * 设置处理时间
     *
     * @param handleTime 处理时间
     */
    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }
}