package com.ntkjxy.wzdjjc.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;

public class Device {
    /**
     * 设备主键
     */
    @Id
    @Column(name = "device_id")
    private Integer deviceId;

    /**
     * 设备编号
     */
    @Column(name = "device_number")
    private String deviceNumber;

    /**
     * 路线
     */
    @Column(name = "route_id")
    private Integer routeId;

    /**
     * 设备名称
     */
    @Column(name = "device_name")
    private String deviceName;

    /**
     * 管理处
     */
    @Column(name = "management_id")
    private Integer managementId;

    /**
     * 设备类型
     */
    @Column(name = "device_type")
    private String deviceType;

    /**
     * 方向编号
     */
    @Column(name = "direction_id")
    private Integer directionId;

    /**
     * 设备IP
     */
    @Column(name = "device_ip")
    private String deviceIp;

    /**
     * 视频端口
     */
    @Column(name = "device_port")
    private String devicePort;

    /**
     * 用户名
     */
    @Column(name = "device_username")
    private String deviceUsername;

    /**
     * 密码
     */
    @Column(name = "device_pwd")
    private String devicePwd;

    /**
     * 通道ID
     */
    @Column(name = "device_channel")
    private String deviceChannel;

    /**
     * 设备位置
     */
    @Column(name = "device_position")
    private String devicePosition;

    /**
     * 所属县城
     */
    @Column(name = "county_id")
    private Integer countyId;

    /**
     * 所属乡镇
     */
    @Column(name = "town_id")
    private Integer townId;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 维度
     */
    private BigDecimal latitude;

    /**
     * 是否标注
     */
    @Column(name = "is_marked")
    private Boolean isMarked;

    /**
     * 设备状态
     */
    @Column(name = "device_status")
    private Integer deviceStatus;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    @Transient
    private County county;
    @Transient
    private Direction direction;
    @Transient
    private Management management;
    @Transient
    private Route route;
    @Transient
    private Town town;

    public Boolean getMarked() {
        return isMarked;
    }

    public void setMarked(Boolean marked) {
        isMarked = marked;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Town getTown() {
        return town;
    }

    public void setTown(Town town) {
        this.town = town;
    }

    /**
     * 获取设备主键
     *
     * @return device_id - 设备主键
     */
    public Integer getDeviceId() {
        return deviceId;
    }

    /**
     * 设置设备主键
     *
     * @param deviceId 设备主键
     */
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 获取设备编号
     *
     * @return device_number - 设备编号
     */
    public String getDeviceNumber() {
        return deviceNumber;
    }

    /**
     * 设置设备编号
     *
     * @param deviceNumber 设备编号
     */
    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    /**
     * 获取路线
     *
     * @return route_id - 路线
     */
    public Integer getRouteId() {
        return routeId;
    }

    /**
     * 设置路线
     *
     * @param routeId 路线
     */
    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    /**
     * 获取设备名称
     *
     * @return device_name - 设备名称
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * 设置设备名称
     *
     * @param deviceName 设备名称
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * 获取管理处
     *
     * @return management_id - 管理处
     */
    public Integer getManagementId() {
        return managementId;
    }

    /**
     * 设置管理处
     *
     * @param managementId 管理处
     */
    public void setManagementId(Integer managementId) {
        this.managementId = managementId;
    }

    /**
     * 获取设备类型
     *
     * @return device_type - 设备类型
     */
    public String getDeviceType() {
        return deviceType;
    }

    /**
     * 设置设备类型
     *
     * @param deviceType 设备类型
     */
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * 获取方向编号
     *
     * @return direction_id - 方向编号
     */
    public Integer getDirectionId() {
        return directionId;
    }

    /**
     * 设置方向编号
     *
     * @param directionId 方向编号
     */
    public void setDirectionId(Integer directionId) {
        this.directionId = directionId;
    }

    /**
     * 获取设备IP
     *
     * @return device_ip - 设备IP
     */
    public String getDeviceIp() {
        return deviceIp;
    }

    /**
     * 设置设备IP
     *
     * @param deviceIp 设备IP
     */
    public void setDeviceIp(String deviceIp) {
        this.deviceIp = deviceIp;
    }

    /**
     * 获取视频端口
     *
     * @return device_port - 视频端口
     */
    public String getDevicePort() {
        return devicePort;
    }

    /**
     * 设置视频端口
     *
     * @param devicePort 视频端口
     */
    public void setDevicePort(String devicePort) {
        this.devicePort = devicePort;
    }

    /**
     * 获取用户名
     *
     * @return device_username - 用户名
     */
    public String getDeviceUsername() {
        return deviceUsername;
    }

    /**
     * 设置用户名
     *
     * @param deviceUsername 用户名
     */
    public void setDeviceUsername(String deviceUsername) {
        this.deviceUsername = deviceUsername;
    }

    /**
     * 获取密码
     *
     * @return device_pwd - 密码
     */
    public String getDevicePwd() {
        return devicePwd;
    }

    /**
     * 设置密码
     *
     * @param devicePwd 密码
     */
    public void setDevicePwd(String devicePwd) {
        this.devicePwd = devicePwd;
    }

    /**
     * 获取通道ID
     *
     * @return device_channel - 通道ID
     */
    public String getDeviceChannel() {
        return deviceChannel;
    }

    /**
     * 设置通道ID
     *
     * @param deviceChannel 通道ID
     */
    public void setDeviceChannel(String deviceChannel) {
        this.deviceChannel = deviceChannel;
    }

    /**
     * 获取设备位置
     *
     * @return device_position - 设备位置
     */
    public String getDevicePosition() {
        return devicePosition;
    }

    /**
     * 设置设备位置
     *
     * @param devicePosition 设备位置
     */
    public void setDevicePosition(String devicePosition) {
        this.devicePosition = devicePosition;
    }

    /**
     * 获取所属县城
     *
     * @return county_id - 所属县城
     */
    public Integer getCountyId() {
        return countyId;
    }

    /**
     * 设置所属县城
     *
     * @param countyId 所属县城
     */
    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    /**
     * 获取所属乡镇
     *
     * @return town_id - 所属乡镇
     */
    public Integer getTownId() {
        return townId;
    }

    /**
     * 设置所属乡镇
     *
     * @param townId 所属乡镇
     */
    public void setTownId(Integer townId) {
        this.townId = townId;
    }

    /**
     * 获取手机号码
     *
     * @return phone - 手机号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置手机号码
     *
     * @param phone 手机号码
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取经度
     *
     * @return longitude - 经度
     */
    public BigDecimal getLongitude() {
        return longitude;
    }

    /**
     * 设置经度
     *
     * @param longitude 经度
     */
    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    /**
     * 获取维度
     *
     * @return latitude - 维度
     */
    public BigDecimal getLatitude() {
        return latitude;
    }

    /**
     * 设置维度
     *
     * @param latitude 维度
     */
    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    /**
     * 获取是否标注
     *
     * @return is_marked - 是否标注
     */
    public Boolean getIsMarked() {
        return isMarked;
    }

    /**
     * 设置是否标注
     *
     * @param isMarked 是否标注
     */
    public void setIsMarked(Boolean isMarked) {
        this.isMarked = isMarked;
    }

    /**
     * 获取设备状态
     *
     * @return device_status - 设备状态
     */
    public Integer getDeviceStatus() {
        return deviceStatus;
    }

    /**
     * 设置设备状态
     *
     * @param deviceStatus 设备状态
     */
    public void setDeviceStatus(Integer deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return update_time - 修改时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置修改时间
     *
     * @param updateTime 修改时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}