package com.ntkjxy.wzdjjc.model;

import javax.persistence.*;

public class County {
    @Id
    @Column(name = "county_id")
    private Integer countyId;

    @Column(name = "county_name")
    private String countyName;

    /**
     * @return county_id
     */
    public Integer getCountyId() {
        return countyId;
    }

    /**
     * @param countyId
     */
    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    /**
     * @return county_name
     */
    public String getCountyName() {
        return countyName;
    }

    /**
     * @param countyName
     */
    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }
}