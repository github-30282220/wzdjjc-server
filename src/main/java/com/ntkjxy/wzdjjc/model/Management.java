package com.ntkjxy.wzdjjc.model;

import javax.persistence.*;

public class Management {
    @Id
    @Column(name = "management_id")
    private Integer managementId;

    @Column(name = "management_name")
    private String managementName;

    /**
     * @return management_id
     */
    public Integer getManagementId() {
        return managementId;
    }

    /**
     * @param managementId
     */
    public void setManagementId(Integer managementId) {
        this.managementId = managementId;
    }

    /**
     * @return management_name
     */
    public String getManagementName() {
        return managementName;
    }

    /**
     * @param managementName
     */
    public void setManagementName(String managementName) {
        this.managementName = managementName;
    }
}