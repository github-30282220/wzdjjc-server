package com.ntkjxy.wzdjjc.model;

import javax.persistence.*;

public class User {
    @Id
    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "user_login")
    private String userLogin;

    @Column(name = "user_pwd")
    private String userPwd;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "user_email")
    private String userEmail;

    @Column(name = "user_phone")
    private String userPhone;

    @Column(name = "management_id")
    private Integer managementId;

    @Column(name = "county_id")
    private Integer countyId;

    @Column(name = "town_id")
    private Integer townId;

    @Column(name = "user_status")
    private Integer userStatus;

    /**
     * @return user_id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return user_login
     */
    public String getUserLogin() {
        return userLogin;
    }

    /**
     * @param userLogin
     */
    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    /**
     * @return user_pwd
     */
    public String getUserPwd() {
        return userPwd;
    }

    /**
     * @param userPwd
     */
    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    /**
     * @return user_name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return user_email
     */
    public String getUserEmail() {
        return userEmail;
    }

    /**
     * @param userEmail
     */
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * @return user_phone
     */
    public String getUserPhone() {
        return userPhone;
    }

    /**
     * @param userPhone
     */
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    /**
     * @return management_id
     */
    public Integer getManagementId() {
        return managementId;
    }

    /**
     * @param managementId
     */
    public void setManagementId(Integer managementId) {
        this.managementId = managementId;
    }

    /**
     * @return county_id
     */
    public Integer getCountyId() {
        return countyId;
    }

    /**
     * @param countyId
     */
    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    /**
     * @return town_id
     */
    public Integer getTownId() {
        return townId;
    }

    /**
     * @param townId
     */
    public void setTownId(Integer townId) {
        this.townId = townId;
    }

    /**
     * @return user_status
     */
    public Integer getUserStatus() {
        return userStatus;
    }

    /**
     * @param userStatus
     */
    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }
}