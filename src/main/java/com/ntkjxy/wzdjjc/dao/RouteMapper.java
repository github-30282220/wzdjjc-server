package com.ntkjxy.wzdjjc.dao;

import com.ntkjxy.wzdjjc.core.Mapper;
import com.ntkjxy.wzdjjc.model.Route;

public interface RouteMapper extends Mapper<Route> {
}