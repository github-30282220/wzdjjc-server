package com.ntkjxy.wzdjjc.dao;

import com.ntkjxy.wzdjjc.core.Mapper;
import com.ntkjxy.wzdjjc.model.Device;

public interface DeviceMapper extends Mapper<Device> {
}