package com.ntkjxy.wzdjjc.dao;

import com.ntkjxy.wzdjjc.core.Mapper;
import com.ntkjxy.wzdjjc.model.Direction;

public interface DirectionMapper extends Mapper<Direction> {
}