package com.ntkjxy.wzdjjc.dao;

import com.ntkjxy.wzdjjc.core.Mapper;
import com.ntkjxy.wzdjjc.model.County;

public interface CountyMapper extends Mapper<County> {
}