package com.ntkjxy.wzdjjc.dao;

import com.ntkjxy.wzdjjc.core.Mapper;
import com.ntkjxy.wzdjjc.model.Event;

import java.util.List;

public interface EventMapper extends Mapper<Event> {
    public List<Event> selectEventByCondition(Event event);

    public List<Event> selectGroupByCondition(Event event);
}