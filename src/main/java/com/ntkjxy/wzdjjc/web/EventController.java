package com.ntkjxy.wzdjjc.web;

import com.ntkjxy.wzdjjc.core.Result;
import com.ntkjxy.wzdjjc.core.ResultGenerator;
import com.ntkjxy.wzdjjc.model.Event;
import com.ntkjxy.wzdjjc.service.EventService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by AndyKong on 2018/01/21.
*/
@RestController
@RequestMapping("/event")
public class EventController {
    @Resource
    private EventService eventService;

    @PostMapping
    public Result add(@RequestBody Event event) {
        eventService.save(event);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        eventService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Event event) {
        eventService.update(event);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Event event = eventService.findById(id);
        return ResultGenerator.genSuccessResult(event);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Event> list = eventService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @PostMapping("/fetchEvent")
    public Result listDetail(@RequestBody Event event) {
        PageHelper.startPage(event.getPage(), event.getSize());
        List<Event> list = eventService.selectEventByCondition(event);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }

    @PostMapping("/fetchEventStatistics")
    public Result listGroup(@RequestBody Event event) {
        PageHelper.startPage(event.getPage(), event.getSize());
        List<Event> list = eventService.selectGroupByCondition(event);
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
