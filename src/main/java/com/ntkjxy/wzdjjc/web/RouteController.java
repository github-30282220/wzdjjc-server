package com.ntkjxy.wzdjjc.web;

import com.ntkjxy.wzdjjc.core.Result;
import com.ntkjxy.wzdjjc.core.ResultGenerator;
import com.ntkjxy.wzdjjc.model.Route;
import com.ntkjxy.wzdjjc.service.RouteService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by AndyKong on 2018/01/21.
*/
@RestController
@RequestMapping("/route")
public class RouteController {
    @Resource
    private RouteService routeService;

    @PostMapping
    public Result add(@RequestBody Route route) {
        routeService.save(route);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        routeService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Route route) {
        routeService.update(route);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Route route = routeService.findById(id);
        return ResultGenerator.genSuccessResult(route);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Route> list = routeService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
