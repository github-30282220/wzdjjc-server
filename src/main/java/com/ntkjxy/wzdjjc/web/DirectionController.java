package com.ntkjxy.wzdjjc.web;

import com.ntkjxy.wzdjjc.core.Result;
import com.ntkjxy.wzdjjc.core.ResultGenerator;
import com.ntkjxy.wzdjjc.model.Direction;
import com.ntkjxy.wzdjjc.service.DirectionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by AndyKong on 2018/01/21.
*/
@RestController
@RequestMapping("/direction")
public class DirectionController {
    @Resource
    private DirectionService directionService;

    @PostMapping
    public Result add(@RequestBody Direction direction) {
        directionService.save(direction);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        directionService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Direction direction) {
        directionService.update(direction);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Direction direction = directionService.findById(id);
        return ResultGenerator.genSuccessResult(direction);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Direction> list = directionService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
