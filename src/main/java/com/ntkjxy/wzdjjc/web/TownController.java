package com.ntkjxy.wzdjjc.web;

import com.ntkjxy.wzdjjc.core.Result;
import com.ntkjxy.wzdjjc.core.ResultGenerator;
import com.ntkjxy.wzdjjc.model.Town;
import com.ntkjxy.wzdjjc.service.TownService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by AndyKong on 2018/01/21.
*/
@RestController
@RequestMapping("/town")
public class TownController {
    @Resource
    private TownService townService;

    @PostMapping
    public Result add(@RequestBody Town town) {
        townService.save(town);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        townService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Town town) {
        townService.update(town);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Town town = townService.findById(id);
        return ResultGenerator.genSuccessResult(town);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Town> list = townService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
