package com.ntkjxy.wzdjjc.web;

import com.ntkjxy.wzdjjc.core.Result;
import com.ntkjxy.wzdjjc.core.ResultGenerator;
import com.ntkjxy.wzdjjc.model.Management;
import com.ntkjxy.wzdjjc.service.ManagementService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
* Created by AndyKong on 2018/01/21.
*/
@RestController
@RequestMapping("/management")
public class ManagementController {
    @Resource
    private ManagementService managementService;

    @PostMapping
    public Result add(@RequestBody Management management) {
        managementService.save(management);
        return ResultGenerator.genSuccessResult();
    }

    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Integer id) {
        managementService.deleteById(id);
        return ResultGenerator.genSuccessResult();
    }

    @PutMapping
    public Result update(@RequestBody Management management) {
        managementService.update(management);
        return ResultGenerator.genSuccessResult();
    }

    @GetMapping("/{id}")
    public Result detail(@PathVariable Integer id) {
        Management management = managementService.findById(id);
        return ResultGenerator.genSuccessResult(management);
    }

    @GetMapping
    public Result list(@RequestParam(defaultValue = "0") Integer page, @RequestParam(defaultValue = "0") Integer size) {
        PageHelper.startPage(page, size);
        List<Management> list = managementService.findAll();
        PageInfo pageInfo = new PageInfo(list);
        return ResultGenerator.genSuccessResult(pageInfo);
    }
}
