package com.ntkjxy.wzdjjc.web;

import com.ntkjxy.wzdjjc.core.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Paths;

/**
 * Created by Andy on 2017/9/5.
 */
@RestController
@RequestMapping("/file")
public class FileUploadController {
    @Autowired
    private Environment env;

    @GetMapping
    public String list() {
        return "aaa";
    }


//    @RequestMapping(value = "{fileName:.+}", method = RequestMethod.GET)
    @GetMapping("getImg")
    @ResponseBody
    public byte[] getImage(HttpServletRequest request, HttpServletResponse response, @RequestParam String fileName) throws IOException {

        FileInputStream fis = null;
        OutputStream os = null;
        byte[] buffer = null;
//        String directory = env.getProperty("dwzzb.paths.uploadedFiles");

        try {
            fis = new FileInputStream(new File(Paths.get(fileName).toString()));
            os = response.getOutputStream();
            int count = 0;
            buffer = new byte[1024 * 8];
            while ((count = fis.read(buffer)) != -1) {
                os.write(buffer, 0, count);
                os.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fis.close();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return buffer;
    }

}
