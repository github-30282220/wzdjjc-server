package com.ntkjxy.wzdjjc;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ntkjxy.wzdjjc.core.ResultGenerator;
import com.ntkjxy.wzdjjc.dao.EventMapper;
import com.ntkjxy.wzdjjc.model.Device;
import com.ntkjxy.wzdjjc.model.Event;
import com.ntkjxy.wzdjjc.model.Town;
import com.ntkjxy.wzdjjc.service.DeviceService;
import com.ntkjxy.wzdjjc.service.EventService;
import com.ntkjxy.wzdjjc.service.TownService;
import com.ntkjxy.wzdjjc.utils.ExcelUtil;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

public class EventTest extends Tester{

    @Resource
    private EventMapper eventMapper;
    @Resource
    private EventService eventService;

    @Resource
    private DeviceService deviceService;

    @Resource
    private TownService townService;


    @Test
    public void testSelectEvent() {
        Event event = new Event();
        event.setEventId(1);
//        Device device = new Device();
//        device.setDeviceNumber("aaa");
//        event.setDevice(device);
        event.setAlarmStartTime(new Date(2018, 0, 19));
        event.setAlarmEndTime(new Date(2018, 0, 22));
        List<Event> eventList = eventMapper.selectEventByCondition(event);
        String eventJson = JSON.toJSONString(eventList);
        System.out.println(eventJson);
    }

    public void testInsertTown() {
        Town town = new Town();
        townService.save(town);
    }

    @Test
    public void testSelectGroupByCondition() {
        Event event = new Event();
        event.setSize(10);
        event.setPage(1);
        event.setGroupType(4);
//        List<Event> eventList = eventMapper.selectGroupByCondition(event);

        PageHelper.startPage(event.getPage(), event.getSize());
        List<Event> list = eventService.selectGroupByCondition(event);
        PageInfo pageInfo = new PageInfo(list);

        String eventJson = JSON.toJSONString(ResultGenerator.genSuccessResult(pageInfo));
        System.out.println(eventJson);
    }

    @Test
    public void testExcelLoader() {
        ExcelUtil excelUtil = new ExcelUtil();
        List<Device> deviceList = excelUtil.readExcelContent("/Users/konghui/Project/MyProjects/wzdjjc/材料/违建报警点位清单-智能分析.xls");
        deviceService.save(deviceList);
    }
}
